﻿$url = '<URL>'
$relativeUrl = ([System.Uri]$url).AbsolutePath

Connect-PnPOnline -Url $url -Credentials (Get-Credential)

Function Create-MasterPageFolder{
	Write-Host 'Creando carpeta B2B en _catalogs/masterpage'
	PnPFolder -Name B2B -Folder _catalogs/masterpage
}

Function Upload-MasterPageFiles{
	Write-Host 'Cargando fichero b2b.master en _catalogs/masterpage/B2B'
	Add-PnPMasterPage -SourceFilePath "_catalogs\B2B\b2b.master" -Title "B2B Master Page" -Description "B2B Master Page" -DestinationFolderHierarchy "B2B"

	Write-Host 'Cargando fichero home.aspx en _catalogs/masterpage/B2B'
	Add-PnPPublishingPageLayout -Title 'Home' -SourceFilePath "_catalogs\B2B\home.aspx" -Description 'Página de inicio' -AssociatedContentTypeID 0x010100C568DB52D9D0A14D9B2FDCC96666E9F2007948130EC3DB064584E219954237AF390064DEA0F50FC8C147B0B6EA0636C4A7D4 -DestinationFolderHierarchy "B2B"

	Write-Host 'Cargando fichero genericlayout.aspx en _catalogs/masterpage/B2B'
	Add-PnPPublishingPageLayout -Title 'Plantilla genérica' -SourceFilePath "_catalogs\B2B\genericlayout.aspx" -Description 'Plantilla generíca' -AssociatedContentTypeID 0x010100C568DB52D9D0A14D9B2FDCC96666E9F2007948130EC3DB064584E219954237AF3900242457EFB8B24247815D688C526CD44D -DestinationFolderHierarchy "B2B"

	Write-Host 'Estableciendo página maestra en b2b.master'
	Set-PnPMasterPage -CustomMasterPageServerRelativeUrl  $relativeUrl + '/_catalogs/masterpage/b2b/b2b.master'
}

Create-MasterPageFolder
Upload-MasterPageFiles