﻿<%@ Page Language="C#" Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=16.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" meta:progid="SharePoint.WebPartPage.Document" %>

<%@ Register TagPrefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<asp:Content Contentplaceholderid="PlaceHolderAdditionalPageHead" runat="server">
</asp:Content>
<asp:Content Contentplaceholderid="PlaceHolderPageTitle" runat="server">
	<SharePointWebControls:FieldValue FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content Contentplaceholderid="PlaceHolderPageTitleInTitleArea" runat="server">
	<SharePointWebControls:FieldValue FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content Contentplaceholderid="PlaceHolderMain" runat="server">
    <div id="search-div" class="row no-gutters">
        <div class="col">
            <div id="search-controls-div">
                <input type="text" class="form-control f-light" id="txtSearch" value="¡Escribe lo que quieras buscar!"/>
                <ul class="list-group" id="searchDropDown"></ul>
                <a href="#" id="btnSearch" data-intro="SearchButton">
                    <span class="icon-search"></span>
                </a>
            </div>
        </div>
    </div>
    <div id="page-header">
        <div class="container">
            <div class="row">
                <div id="page-image" class="col">
                    <img src="/sites/B2BFinder.OCEWU/style library/b2b/images/favourites.png" class="img-fluid"/>
                </div>
                <div id="page-title" class="col-9">
                    <h2><span data-key="My" class="f-xlight">Mis</span>&nbsp;<span data-key="favourites_" class="f-regular">favoritos_</span></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <h3 class="mb-5"><span data-key="FavouritesLight" class="f-xlight">Desde esta página puedes ver y gestionar de forma sencilla</span>&nbsp;<span data-key="FavouritesRegular" class="f-regular">todos tus documentos favoritos</span></h3>
        <table class="table table-hover" id="favouritesTable">
            <thead>
                <tr>
                    <th scope="col"><span data-key="Type">Tipo</span></th>
                    <th scope="col"><span data-key="Document">Documento</span></th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
        
            </tbody>
        </table>
    </div>
</asp:Content>
