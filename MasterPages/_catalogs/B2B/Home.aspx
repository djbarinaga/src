﻿<%@ Page Language="C#" Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=16.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" meta:progid="SharePoint.WebPartPage.Document" %>

<%@ Register TagPrefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<asp:Content contentplaceholderid="PlaceHolderAdditionalPageHead" runat="server">
	<script src='<asp:Literal runat="server" Text="<%$SPUrl:~sitecollection/Style Library/B2B/js/carousel.js%>" />'></script>
</asp:Content>
<asp:Content contentplaceholderid="PlaceHolderPageTitle" runat="server">
	
</asp:Content>
<asp:Content contentplaceholderid="PlaceHolderPageTitleInTitleArea" runat="server">
	<SharePointWebControls:FieldValue FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content contentplaceholderid="PlaceHolderMain" runat="server">
    <PublishingWebControls:EditModePanel runat="server" CssClass="edit-mode-panel title-edit">
		<SharePointWebControls:TextField runat="server" FieldName="Title"/>
	</PublishingWebControls:EditModePanel>
    <div id="home">
        <div id="search-div" class="row no-gutters">
            <div class="col">
                <h1><span class="f-xlight">B2B</span><span class="f-regular">Finder</span></h1>
                <div id="search-controls-div">
                    <input type="text" class="form-control f-light" id="txtSearch" value="¡Escribe lo que quieras buscar!"/>
                    <ul class="list-group" id="searchDropDown"></ul>
                    <a href="#" id="btnSearch" data-intro="SearchButton">
                        <span class="icon-search"></span>
                    </a>
                </div>
            </div>
        </div>
        <div class="container mt-5">
            <div id="carousel" class="carousel slide carousel-fade" data-ride="carousel" data-intro="Carousel">
              <div class="carousel-inner">
              </div>
            </div>
            <div class="module">
                <h3 class="module-title" data-key="HighlightDocuments"></h3>
                <WebPartPages:WebPartZone runat="server" Title="Zona principal" ID="WebPartZone7"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone>
	        </div>
        </div>
        <div class="modal fade" tabindex="-1" id="welcomeModal" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <p>Bienvenido a</p>
                        <h2 class="fg-blue01"><strong>B2B</strong> Finder</h2>
                        <p>¿Deseas activar el agente para dar un paseo por la aplicación?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-gray" id="btnCancelWizard">No</button>
                        <button type="button" class="btn btn-blue09" id="btnLaunchWizard">Sí, dar un paseo</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <SharePointWebControls:ScriptBlock runat="server">
	    if(typeof(MSOLayout_MakeInvisibleIfEmpty) == 'function'){MSOLayout_MakeInvisibleIfEmpty();}

    </SharePointWebControls:ScriptBlock>
</asp:Content>
