﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint.Client;

namespace MigracionB2B
{
    class Program
    {
        public class FileInfo
        {
            public string File { get; set; }
            public string Url { get; set; }
            public string Folder { get; set; }
            public string Library { get; set; }
            public string CreatedBy { get; set; }
            public DateTime CreatedDate { get; set; }
            public string ModifiedBy { get; set; }
            public DateTime ModifiedDate { get; set; }
        }

        static List<FileInfo> files = new List<FileInfo>();

        static void Main(string[] args)
        {
            string siteUrl = GetArgument(args, "site");
            string user = GetArgument(args, "username");
            string pwd = GetArgument(args, "password");
            string lists = GetArgument(args, "lists");
            string[] sourceLists = lists.Split(',');

            ClientContext context = new ClientContext(siteUrl);
            context.Credentials = new SharePointOnlineCredentials(user, GetSecureString(pwd));

            foreach (string sourceList in sourceLists)
            {
                List list = context.Web.Lists.GetByTitle(sourceList);
                Folder rootFolder = list.RootFolder;

                context.Load(list);
                context.Load(rootFolder);

                Console.WriteLine($"Conectando con {siteUrl}...");
                context.ExecuteQuery();

                GetFiles(rootFolder, context, sourceList);
            }

            Console.WriteLine("");
            Console.WriteLine("Generando fichero CSV...");
            string path = AppDomain.CurrentDomain.BaseDirectory;
            path += "\\documents.csv";

            using (StreamWriter writer = new StreamWriter(path))
            {
                writer.WriteLine("Library|Folder|File|CreatedBy|CreatedDate|ModifiedBy|ModifiedDate");
                writer.Flush();

                foreach (FileInfo file in files)
                {
                    writer.WriteLine($"{file.Library}|{file.Folder}|{file.File}|{file.CreatedBy}|{file.CreatedDate}|{file.ModifiedBy}|{file.ModifiedDate}");
                    writer.Flush();
                }
            }
        }

        private static void GetFiles(Folder mainFolder, ClientContext clientContext, string library)
        {
            clientContext.Load(mainFolder, k => k.Files, k => k.Folders);
            clientContext.ExecuteQuery();

            foreach (var folder in mainFolder.Folders)
            {
                GetFiles(folder, clientContext, library);
            }

            int counter = 0;
            int itemCount = mainFolder.Files.Count;

            Console.WriteLine($"Ficheros en {mainFolder.ServerRelativeUrl}: {itemCount}");

            if (itemCount > 0)
            {
                Console.WriteLine($"Ficheros en {mainFolder.ServerRelativeUrl}: {itemCount}");
                Console.Write("\tObteniendo ficheros de {0}...\r", mainFolder.Name);
            }
            else
            {
                files.Add(new FileInfo()
                {
                    Library = library,
                    Folder = mainFolder.ServerRelativeUrl
                });
            }

            foreach (var file in mainFolder.Files)
            {
                var fileRef = file.ServerRelativeUrl;
                var fileName = file.Name;
                string author = "";
                string editor = "";

                try
                {
                    clientContext.Load(file.Author);
                    clientContext.Load(file.ModifiedBy);
                    clientContext.ExecuteQuery();

                    author = file.Author.Email;
                    editor = file.ModifiedBy.Email;
                }
                catch (Exception ex) { }

                files.Add(new FileInfo()
                {
                    Library = library,
                    Folder = mainFolder.ServerRelativeUrl,
                    File = fileName,
                    Url = fileRef,
                    CreatedBy = author,
                    CreatedDate = file.TimeCreated,
                    ModifiedBy = editor,
                    ModifiedDate = file.TimeLastModified
                });

                counter++;

                int percentage = (counter * 100) / itemCount;

                if (counter < itemCount)
                    Console.Write("\tObteniendo ficheros de {0}... {1}%\r", mainFolder.Name, percentage);
                else
                    Console.WriteLine("\tObteniendo ficheros de {0}... {1}%\r", mainFolder.Name, percentage);
            }

        }

        private static SecureString GetSecureString(string pwd)
        {
            char[] chars = pwd.ToCharArray();
            SecureString securePassword = new SecureString();

            for (int i = 0; i < chars.Length; i++)
            {
                securePassword.AppendChar(chars[i]);
            }

            return securePassword;
        }

        static string GetArgument(string[] args, string key)
        {
            string completeKey = "-" + key;
            string value = "";

            for (int i = 0; i < args.Length; i++)
            {
                if(args[i].Equals(completeKey, StringComparison.InvariantCultureIgnoreCase)){
                    value = args[i + 1];
                    break;
                }
            }

            if (string.IsNullOrEmpty(value))
            {
                throw new Exception($"Proporcione un valor para el comando {completeKey}");
            }

            return value;
        }
    }
}
