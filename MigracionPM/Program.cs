﻿using CsvHelper;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MigracionPM
{
    class Program
    {
        public class FileInfo
        {
            public string File { get; set; }
            public string Url { get; set; }
            public string Folder { get; set; }
            public string Library { get; set; }
            public string CreatedBy { get; set; }
            public DateTime CreatedDate { get; set; }
            public string ModifiedBy { get; set; }
            public DateTime ModifiedDate { get; set; }
        }

        static List<FileInfo> files = new List<FileInfo>();
        static string path = @"C:\Proyectos\Avantgarde\Telefónica\Desarrollo\Migracion\Logos";

        static void Main(string[] args)
        {
            

            ClientContext context = new ClientContext("https://logos.telefonicabusinesssolutions.com/pm");
            context.Credentials = new System.Net.NetworkCredential("administrador", "#Npmmamt01", "inav");

            string[] docLibraries = new string[] { "Workshops / Webinars", "Customer Material", "Internal Material" };

            foreach(string docLibrary in docLibraries)
            {
                Console.WriteLine("***********************");
                Console.WriteLine($"Procesando {docLibrary}");

                List list = context.Web.Lists.GetByTitle(docLibrary);
                Folder rootFolder = list.RootFolder;

                context.Load(list);
                context.Load(rootFolder);
                context.ExecuteQuery();

                GetFiles(rootFolder, context, docLibrary);
            }

            Console.WriteLine("");
            Console.WriteLine("Generando fichero CSV...");
            using (StreamWriter writer = new StreamWriter(@"C:\Proyectos\Avantgarde\Telefónica\Desarrollo\Reports\PMDocuments.csv"))
            {
                writer.WriteLine("Library|Folder|File|CreatedBy|CreatedDate|ModifiedBy|ModifiedDate");
                writer.Flush();

                foreach (FileInfo file in files)
                {
                    writer.WriteLine($"{file.Library}|{file.Folder}|{file.File}|{file.CreatedBy}|{file.CreatedDate}|{file.ModifiedBy}|{file.ModifiedDate}");
                    writer.Flush();
                }
            }

            Console.ReadLine();
        }


        private static void GetFiles(Folder mainFolder, ClientContext clientContext, string library)
        {
            clientContext.Load(mainFolder, k => k.Files, k => k.Folders);
            clientContext.ExecuteQuery();

            foreach (var folder in mainFolder.Folders)
            {
                GetFiles(folder, clientContext, library);
            }

            int counter = 0;
            int itemCount = mainFolder.Files.Count;


            if (itemCount > 0)
            {
                Console.WriteLine($"\tFicheros en {mainFolder.Name}: {itemCount}");
                //Console.Write("\t\tObteniendo ficheros de {0}...", mainFolder.Name);
            }
            //else
            //{
            //    files.Add(new FileInfo()
            //    {
            //        Library = library,
            //        Folder = mainFolder.ServerRelativeUrl
            //    });
            //}

            foreach (var file in mainFolder.Files)
            {
                var fileRef = file.ServerRelativeUrl;
                var fileName = file.Name;
                //string author = "";
                //string editor = "";

                try
                {
                    //clientContext.Load(file.Author);
                    //clientContext.Load(file.ModifiedBy);
                    //clientContext.ExecuteQuery();

                    //author = file.Author.Email;
                    //editor = file.ModifiedBy.Email;
                    Console.WriteLine("\t\tDescargando {0}", fileName);
                    FileInformation fileInfo = Microsoft.SharePoint.Client.File.OpenBinaryDirect(clientContext, fileRef);
                    string filePath = Path.Combine(path, fileName);

                    using (FileStream fileStream = System.IO.File.Create(filePath))
                    {
                        fileInfo.Stream.CopyTo(fileStream);
                    }
                }
                catch (Exception ex) {
                    Console.WriteLine(ex.ToString());
                }

                //files.Add(new FileInfo()
                //{
                //    Library = library,
                //    Folder = mainFolder.ServerRelativeUrl,
                //    File = fileName,
                //    Url = fileRef,
                //    CreatedBy = author,
                //    CreatedDate = file.TimeCreated,
                //    ModifiedBy = editor,
                //    ModifiedDate = file.TimeLastModified
                //});

                //counter++;

                //int percentage = (counter * 100) / itemCount;

                //if (counter < itemCount)
                //    Console.Write("\t\tObteniendo ficheros de {0}... {1}%\r", mainFolder.Name, percentage);
                //else
                //    Console.WriteLine("\t\tObteniendo ficheros de {0}... {1}%\r", mainFolder.Name, percentage);
            }

        }
    }
}
