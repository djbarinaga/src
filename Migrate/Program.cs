﻿using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace Migrate
{
    class Program
    {
        static string tempFolder = AppDomain.CurrentDomain.BaseDirectory + "\\temp";
        static string b2bUrl = "https://telefonicacorp.sharepoint.com/sites/B2BFinder.OCEWU/es-es";
        static string pass = "";
        static string email = "";
        static string logFile = $".{DateTime.Now.ToString("ddMMyyyyHHmm")}.log";

        static StreamWriter log = null;

        static void Main(string[] args)
        {
            if (!System.IO.File.Exists(logFile))
            {
                log = new StreamWriter(logFile);
            }
            else
            {
                log = System.IO.File.AppendText(logFile);
            }

            //try
            //{
            //    Console.WriteLine($"Conectando a {b2bUrl}");
            //    Console.WriteLine();

            //    Console.Write("Enter your email: ");

            //    do
            //    {
            //        ConsoleKeyInfo key = Console.ReadKey(true);
            //        // Backspace Should Not Work
            //        if (key.Key != ConsoleKey.Backspace && key.Key != ConsoleKey.Enter)
            //        {
            //            email += key.KeyChar;
            //            Console.Write(key.KeyChar);
            //        }
            //        else
            //        {
            //            if (key.Key == ConsoleKey.Backspace && email.Length > 0)
            //            {
            //                email = email.Substring(0, (email.Length - 1));
            //                Console.Write("\b \b");
            //            }
            //            else if (key.Key == ConsoleKey.Enter)
            //            {
            //                Console.WriteLine();
            //                break;
            //            }
            //        }
            //    } while (true);

            //    Console.Write("Enter your password: ");

            //    do
            //    {
            //        ConsoleKeyInfo key = Console.ReadKey(true);
            //        // Backspace Should Not Work
            //        if (key.Key != ConsoleKey.Backspace && key.Key != ConsoleKey.Enter)
            //        {
            //            pass += key.KeyChar;
            //            Console.Write("*");
            //        }
            //        else
            //        {
            //            if (key.Key == ConsoleKey.Backspace && pass.Length > 0)
            //            {
            //                pass = pass.Substring(0, (pass.Length - 1));
            //                Console.Write("\b \b");
            //            }
            //            else if (key.Key == ConsoleKey.Enter)
            //            {
            //                Console.WriteLine();
            //                break;
            //            }
            //        }
            //    } while (true);


            //    ReadCsv();

            //    //if (CheckConnection())
            //    //    ReadCsv();
            //    //else
            //    //    Console.ReadLine();
            //}
            //catch(Exception ex)
            //{
            //    Console.WriteLine(ex.ToString());
            //}
            //finally
            //{
            //    log.Close();
            //    log.Dispose();
            //}

            ReadCsv();
        }

        private static bool CheckConnection()
        {
            try
            {
                ClientContext b2bContext = new ClientContext(b2bUrl);
                b2bContext.Credentials = new SharePointOnlineCredentials(email, GetSecureString(pass));

                b2bContext.ExecuteQuery();

                Console.WriteLine($"{DateTime.Now.ToShortTimeString()}\tConexión establecida");
                log.WriteLine($"{DateTime.Now.ToShortTimeString()}\tConexión establecida");
                log.Flush();

                return true;
            }
            catch(Exception ex)
            {
                Console.WriteLine($"{DateTime.Now.ToShortTimeString()}\tNo se ha podido establecer la conexión con {b2bUrl}. {ex.ToString()}");
                log.WriteLine($"{DateTime.Now.ToShortTimeString()}\tNo se ha podido establecer la conexión con {b2bUrl}. {ex.ToString()}");
                log.Flush();

                return false;
            }
        }

        private static void ReadCsv()
        {
            string[] files = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory + "\\csv");
            foreach(string file in files)
            {
                TransformCsv(file);
            }
        }

        private static void TransformCsv(string file)
        {
            using(StreamReader reader = new StreamReader(file))
            {
                int counter = 0;
                string line = reader.ReadLine();
                System.Data.DataTable dt = new System.Data.DataTable();

                while (line != null)
                {
                    Console.Write($"{DateTime.Now.ToShortTimeString()}\tProcesando fichero {Path.GetFileName(file)}. Línea: {counter}\r");
                    log.WriteLine($"{DateTime.Now.ToShortTimeString()}\tProcesando fichero {Path.GetFileName(file)}. Línea: {counter}");
                    log.Flush();

                    if (counter == 0)
                    {
                        string[] fields = line.Split(';');
                        foreach(string field in fields)
                        {
                            dt.Columns.Add(field);
                        }
                    }
                    else
                    {
                        string[] values = line.Split(';');

                        DataRow dr = dt.NewRow();
                        for(int i = 0; i < values.Length; i++)
                        {
                            dr[i] = values[i];
                        }

                        dt.Rows.Add(dr);
                    }

                    dt.AcceptChanges();

                    line = reader.ReadLine();
                    counter++;
                }

                Console.WriteLine($"{DateTime.Now.ToShortTimeString()}\tFichero {Path.GetFileName(file)} procesado. Líneas: {counter}");
                log.WriteLine($"{DateTime.Now.ToShortTimeString()}\tFichero {Path.GetFileName(file)} procesado. Líneas: {counter}");
                log.Flush();

                CopyDocument(dt);
            }
        }

        private static void CopyDocument(DataTable dt)
        {
            Console.WriteLine($"{DateTime.Now.ToShortTimeString()}\tObteniendo ficheros a migrar");
            log.WriteLine($"{DateTime.Now.ToShortTimeString()}\tObteniendo ficheros a migrar");
            log.Flush();

            DataView dv = new DataView(dt);
            dv.RowFilter = "[Familia de producto] is not null";

            Console.WriteLine($"{DateTime.Now.ToShortTimeString()}\tFicheros a migrar {dv.Count}");
            log.WriteLine($"{DateTime.Now.ToShortTimeString()}\tFicheros a migrar {dv.Count}");
            log.Flush();

            foreach (DataRowView drv in dv)
            {
                DownloadDocument(drv);
            }
        }

        private static void DownloadDocument(DataRowView drv)
        {
            try
            {
                string url = $"{drv["Folder"]}/{drv["File"]}";

                Console.WriteLine($"{DateTime.Now.ToShortTimeString()}\tDescargando {url}");
                log.WriteLine($"{DateTime.Now.ToShortTimeString()}\tDescargando {url}");
                log.Flush();

                ClientContext context = new ClientContext("https://logos.telefonicabusinesssolutions.com/pm");
                context.Credentials = new System.Net.NetworkCredential("WUEDJI01", "Telefonica@19", "tefgad");

                Microsoft.SharePoint.Client.File file = context.Web.GetFileByServerRelativeUrl(url);

                context.Load(file);
                context.ExecuteQuery();

                FileInformation fileInfo = Microsoft.SharePoint.Client.File.OpenBinaryDirect(context, file.ServerRelativeUrl);

                context.ExecuteQuery();

                string filePath = Path.Combine(tempFolder, file.Name);

                using (FileStream fileStream = new System.IO.FileStream(filePath, System.IO.FileMode.Create))
                {
                    fileInfo.Stream.CopyTo(fileStream);
                }

                //UploadDocument(filePath, drv);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                log.WriteLine(ex.ToString());
                log.Flush();
            }
        }

        private static void UploadDocument(string filePath, DataRowView drv)
        {
            Console.WriteLine($"{DateTime.Now.ToShortTimeString()}\tCargando {Path.GetFileName(filePath)}");
            log.WriteLine($"{DateTime.Now.ToShortTimeString()}\tCargando {Path.GetFileName(filePath)}");
            log.Flush();

            ClientContext b2bContext = new ClientContext(b2bUrl);
            b2bContext.Credentials = new SharePointOnlineCredentials(email, GetSecureString(pass));

            List documentLibrary = b2bContext.Web.Lists.GetByTitle("Documentos B2B");

            FileCreationInformation fileCreationInformation = new FileCreationInformation();
            fileCreationInformation.Overwrite = true;
            fileCreationInformation.Content = System.IO.File.ReadAllBytes(filePath);
            fileCreationInformation.Url = Path.GetFileName(filePath);

            Microsoft.SharePoint.Client.File uploadFile = documentLibrary.RootFolder.Files.Add(fileCreationInformation);

            try
            {
                b2bContext.Load(documentLibrary);
                b2bContext.Load(uploadFile);
                b2bContext.ExecuteQuery();

                Console.WriteLine($"{DateTime.Now.ToShortTimeString()}\tFichero {Path.GetFileName(filePath)} cargado");
                log.WriteLine($"{DateTime.Now.ToShortTimeString()}\tFichero {Path.GetFileName(filePath)} cargado");

                Console.WriteLine($"{DateTime.Now.ToShortTimeString()}\tEstableciendo metadatos");
                log.WriteLine($"{DateTime.Now.ToShortTimeString()}\tEstableciendo metadatos");

                Console.WriteLine($"\tContentType: {drv["Tipo de documento"]}");
                log.WriteLine($"\tContentType: {drv["Tipo de documento"]}");

                Console.WriteLine($"\tProductFamily: {drv["Familia de producto"]}");
                log.WriteLine($"\tProductFamily: {drv["Familia de producto"]}");

                Console.WriteLine($"\tProduct: {drv["Producto"]}");
                log.WriteLine($"\tProduct: {drv["Producto"]}");

                Console.WriteLine($"\tCountry: {drv["Pais"]}");
                log.WriteLine($"\tCountry: {drv["Pais"]}");

                Console.WriteLine($"\tDocumentUse: {drv["Uso"]}");
                log.WriteLine($"\tDocumentUse: {drv["Uso"]}");

                Console.WriteLine($"\tCreatedBy: {drv["CreatedBy"]}");
                log.WriteLine($"\tCreatedBy: {drv["CreatedBy"]}");

                log.Flush();

                ListItem item = uploadFile.ListItemAllFields;
                item["ProductFamily"] = drv["Familia de producto"];
                item["Product"] = drv["Producto"];
                item["Country"] = drv["Pais"];
                item["DocumentUse"] = drv["Uso"];

                if(drv["Tipo de documento"] != DBNull.Value && drv["Tipo de documento"] != null && drv["Tipo de documento"].ToString() != "")
                {
                    string ctId = GetContentTypeId(b2bContext, drv["Tipo de documento"].ToString());
                    item["ContentTypeId"] = ctId;
                }

                if (drv["CreatedBy"] != DBNull.Value && drv["CreatedBy"] != null && drv["CreatedBy"].ToString() != "")
                {
                    int userId = GetUserId(b2bContext, drv["CreatedBy"].ToString());
                    item["Author"] = userId;
                }

                item.Update();
                b2bContext.ExecuteQuery();

                Console.WriteLine($"{DateTime.Now.ToShortTimeString()}\tMetadatos modificados");
                log.WriteLine($"{DateTime.Now.ToShortTimeString()}\tMetadatos modificados");
                log.Flush();

                string folderPath = $"{drv["Familia de producto"]}/{drv["Producto"]}/{drv["Tipo de documento"]}";
                Folder folder = CreateFolder(b2bContext, documentLibrary, folderPath);

                Console.WriteLine($"{DateTime.Now.ToShortTimeString()}\tMoviendo archivo {Path.GetFileName(filePath)} a {folder.ServerRelativeUrl}");
                log.WriteLine($"{DateTime.Now.ToShortTimeString()}\tMoviendo archivo {Path.GetFileName(filePath)} a {folder.ServerRelativeUrl}");
                log.Flush();

                uploadFile.MoveTo($"{folder.ServerRelativeUrl}/{Path.GetFileName(filePath)}", MoveOperations.Overwrite);
                b2bContext.ExecuteQuery();

                DeleteTempFile(filePath);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{DateTime.Now.ToShortTimeString()}\tError subiendo el archivo {filePath}. {ex.ToString()}");
                log.WriteLine($"{DateTime.Now.ToShortTimeString()}\tError subiendo el archivo {filePath}. {ex.ToString()}");
                log.Flush();
            }
        }

        private static string GetContentTypeId(ClientContext context, string contentType)
        {
            Console.WriteLine($"{DateTime.Now.ToShortTimeString()}\tObteniendo ID para el tipo de contenido {contentType}");
            log.WriteLine($"{DateTime.Now.ToShortTimeString()}\tObteniendo ID para el tipo de contenido {contentType}");
            log.Flush();

            ContentTypeCollection contentTypes = context.Web.AvailableContentTypes;
            context.Load(contentTypes);

            string ctId = string.Empty;

            foreach(ContentType ct in contentTypes)
            {
                if(ct.Name.Equals(contentType, StringComparison.InvariantCultureIgnoreCase))
                {
                    ctId = ct.Id.ToString();
                }
            }

            return ctId;
        }

        private static int GetUserId(ClientContext context, string email)
        {
            int id = -1;

            var result = Microsoft.SharePoint.Client.Utilities.Utility.ResolvePrincipal(context, context.Web, email, Microsoft.SharePoint.Client.Utilities.PrincipalType.User, Microsoft.SharePoint.Client.Utilities.PrincipalSource.All, null, true);
            context.ExecuteQuery();
            if (result != null)
            {
                var user = context.Web.EnsureUser(result.Value.LoginName);
                context.Load(user);
                context.ExecuteQuery();

                id = user.Id;
            }

            return id;
        }

        public static Folder CreateFolder(ClientContext context, List documentLibrary, string fullFolderPath)
        {
            Console.WriteLine($"{DateTime.Now.ToShortTimeString()}\tCreando carpeta {fullFolderPath}");
            log.WriteLine($"{DateTime.Now.ToShortTimeString()}\tCreando carpeta {fullFolderPath}");
            log.Flush();

            if (string.IsNullOrEmpty(fullFolderPath))
                throw new ArgumentNullException("fullFolderPath");

            return CreateFolderInternal(context, documentLibrary.RootFolder, fullFolderPath);
        }


        private static Folder CreateFolderInternal(ClientContext context, Folder parentFolder, string fullFolderPath)
        {
            var folderUrls = fullFolderPath.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            string folderUrl = folderUrls[0];
            var curFolder = parentFolder.Folders.Add(folderUrl);
            context.Load(curFolder);
            context.ExecuteQuery();

            if (folderUrls.Length > 1)
            {
                var folderPath = string.Join("/", folderUrls, 1, folderUrls.Length - 1);
                return CreateFolderInternal(context, curFolder, folderPath);
            }
            return curFolder;
        }

        private static void DeleteTempFile(string filePath)
        {
            Console.WriteLine($"{DateTime.Now.ToShortTimeString()}\tEliminando fichero {filePath}");
            log.WriteLine($"{DateTime.Now.ToShortTimeString()}\tEliminando fichero {filePath}");
            log.Flush();

            System.IO.File.Delete(filePath);
        }

        private static SecureString GetSecureString(string pwd)
        {
            char[] chars = pwd.ToCharArray();
            SecureString securePassword = new SecureString();

            for (int i = 0; i < chars.Length; i++)
            {
                securePassword.AppendChar(chars[i]);
            }

            return securePassword;
        }
    }
}
