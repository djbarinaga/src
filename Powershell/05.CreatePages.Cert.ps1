﻿$url = '<URL>/es-es'

Connect-PnPOnline -Url $url -Credentials (Get-Credential)

Write-Host 'Creando Página Home'
Add-PnPPublishingPage -PageName 'Home' -Title 'Telefónica Business Solutions' -PageTemplateName 'B2B/Home'

Write-Host 'Creando Página Favoritos'
Add-PnPPublishingPage -PageName 'Favoritos' -Title 'Mis favoritos' -PageTemplateName 'B2B/GenericLayout'

Write-Host 'Estableciendo página de inicio'
Set-PnPHomePage -RootFolderRelativeUrl 'Paginas/Home.aspx'