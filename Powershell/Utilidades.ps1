﻿#DESARROLLO
Write-Host 'Conectando...'
$url = "/sites/B2BFinder.OCEWU/es-es"
$user = 'iago.cid@avantgardeit.es'
$pwd = convertto-securestring -String 'Hochtief2019.' -AsPlainText -Force

$cred = new-object -typename System.Management.Automation.PSCredential -argumentlist $user, $pwd

Connect-PnPOnline -Url $url -Credentials $cred
#FIN DESARROLLO

<#Get-PnPContentType | 
		Foreach-Object {
			Write-Host "Eliminando" $_.Name
			Remove-PnPContentType-Identity $_.Name -Force
		}

Get-PnPField -Group 'B2B' | 
		Foreach-Object {
			Write-Host "Eliminando" $_.Title
			Remove-PnPField -Identity $_.Title -Force
		}#>

$cTypes = Get-PnPContentType -List 'Documentos B2B'
$cTypes.Count

Get-ChildItem "C:\Proyectos\Avantgarde\Telefónica\Documentacion" | 
		Foreach-Object {
			$index = Get-Random -Maximum $cTypes.Count

			$filePath = $_.FullName
			$fileUrl = "/sites/B2BFinder.OCEWU/es-es/Documentos B2B/" + $_.Name

			Write-Host 'Agregando archivo '$_.Name' a /sites/B2BFinder.OCEWU/es-es/Documentos B2B'
			Add-PnPFile -Path $filePath -Folder "Documentos B2B"

			Write-Host 'Protegiendo archivo ' $fileUrl
			Set-PnPFileCheckedIn -Url $fileUrl -CheckinType Major
		}