﻿(function ($) {
    $.fn.documentInfo = function () {
        var $this = this;
        SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () {
            var relativeUrl = $($this).data('url');

            var clientContext = SP.ClientContext.get_current();
            var file = clientContext.get_web().getFileByServerRelativeUrl(relativeUrl);
            clientContext.load(file, 'ListItemAllFields');


            clientContext.executeQueryAsync(
                function () {
                    var docEnablers = $($this).find('.doc-enablers');

                    var result = file.get_listItemAllFields();
                    var item = result.get_fieldValues();
                    
                    if (item["Title"] != null) {
                        $($this).find('.card-title').text(item["Title"]);
                    }

                    if (item["DocumentUse"] != null) {
                        $($this).find('.doc-use').text(item["DocumentUse"].toLowerCase());
                    }


                    $($this).find('.card-title').removeClass('hide');
                },
                function (sender, args) {
                    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                }
            );
        });
    };
}(jQuery));