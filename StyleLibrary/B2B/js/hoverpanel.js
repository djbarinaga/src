﻿(function ($) {
    $.fn.hoverpanel = function () {
        var $this = this;
        var itemId = $($this).data('id');
        var relativeUrl = $($this).data('url');
        var favourites = $($this).find('[data-command="favourite"] span')[0];
        var favouritesIcon = $(favourites).attr('class');

        var clientContext = SP.ClientContext.get_current();
        var file = clientContext.get_web().getFileByServerRelativeUrl(relativeUrl);
        clientContext.load(file, 'ListItemAllFields');
        

        clientContext.executeQueryAsync(
            function () {
                var html = $('#hoverpanel').html();
                var auxDiv = $('<div/>');
                var contentType = '';
                var userEmail = '';

                var result = file.get_listItemAllFields();

                var documentUse = (result.get_item('DocumentUse') != null ? result.get_item('DocumentUse') : '');
                var country = (result.get_item('Country') != null ? result.get_item('Country') : '');
                userEmail = result.get_item('Author').get_email();
                var userName = result.get_item('Author').get_lookupValue();
                var description = ''; //(result.get_item('Description') != null ? result.get_item('Description') : '');
                contentType = result.get_item('ContentTypeId').toString();


                html = html.replace('[title]', $($this).find('.card-title').text());
                html = html.replace('[use]', documentUse);
                html = html.replace('[country]', country);
                html = html.replace('[email]', userEmail);
                html = html.replace('[author]', userName);
                html = html.replace('[author-picture]', '/_layouts/15/userphoto.aspx?accountname=' + userEmail);
                html = html.replace('[description]', description);
                html = html.replace('[favourite]', favouritesIcon);

                auxDiv.append(html);

                translateAll(auxDiv);

                var popover = $($this).popover({
                    container: 'body',
                    content: auxDiv.html(),
                    template: '<div class="popover hover-panel" role="tooltip"><div class="arrow"></div><div class="popover-body"></div></div>',
                    html: true,
                    trigger: 'manual'
                }).on('mouseenter', function () {
                    var _this = this;
                    $(this).popover('show');
                    $('.popover').on('mouseleave', function () {
                        $(_this).popover('hide');
                    });
                }).on('mouseleave', function () {
                    var _this = this;
                    setTimeout(function () {
                        if (!$('.popover:hover').length) {
                            $(_this).popover('hide');
                        }
                    }, 100);
                });

                $(popover).on('show.bs.popover', function () {
                    getRelatedDocuments(contentType, itemId);
                    SP.SOD.executeFunc('userprofile', 'SP.UserProfiles.PeopleManager', function () {
                        getUserProperties(userEmail);
                    });
                    
                });
            },
            function (sender, args) {
                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            }
        );
    };

    function getRelatedDocuments(contentType, itemId) {
        var clientContext = SP.ClientContext.get_current();
        var list = clientContext.get_web().get_lists().getByTitle('Documentos B2B');
        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml("<View><Query><Where><And><Eq><FieldRef Name='ContentTypeId' /><Value Type='ContentTypeId'>" + contentType + "</Value></Eq><Neq><FieldRef Name='ID' /><Value Type='Counter'>" + itemId + "</Value></Neq></And></Where></Query><RowLimit Paged='False'>5</RowLimit></View>");

        var items = list.getItems(camlQuery);
        clientContext.load(items);

        clientContext.executeQueryAsync(
            function () {
                var listItemEnumerator = items.getEnumerator();
                var html = '';
                while (listItemEnumerator.moveNext()) {
                    var result = listItemEnumerator.get_current();
                    var extension = result.get_item('FileLeafRef').split('.')[1];
                    var fileName = result.get_item('FileLeafRef').replace('.' + extension, '');
                    html += '<li class="list-group-item">';
                    html += getDocumentIcon(extension);
                    html += '<a href="' + result.get_item('FileRef') + '" target="_blank" class="anchor-icon">' + fileName + '</a>';
                    html += '</li>';
                }
                $('.hover-panel').find('#preloading').remove();
                $('.hover-panel').find('.list-group').empty();
                $('.hover-panel').find('.list-group').append(html);
                $('.hover-panel').find('.list-group').fadeIn();
            },
            function (sender, args) {
                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            }
        );
    }

    function getUserProperties(userEmail) {
        var userProfileProperties;
        var loginName = 'i:0#.f|membership|' + userEmail;
        var clientContext = new SP.ClientContext.get_current();
        var peopleManager = new SP.UserProfiles.PeopleManager(clientContext);

        var propertyName = "JobTitle";

        userProfileProperties = peopleManager.getUserProfilePropertyFor(loginName, propertyName);

        clientContext.executeQueryAsync(
            function () {
                $('.hover-panel').find('#author-title').text(userProfileProperties.get_value());
            },
            function (sender, args) {
                console.log("Error: " + args.get_message());
            }
        );
    }
}(jQuery));