﻿var targetDocuments = new Array();
var documentTypeImages = new Array();
var documentTypeDescriptions = new Array();
var documentTypes = new Array();
var documentUploaded = new Array();
var products = new Array();
var productFamily = new Array();
var enablers = new Array();
var permissions = new Array();
var maxLength = 16;
var use = ["Interno", "Externo"];
var currentIndex = 0;


$(document).ready(function () {
    SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () {
        loadEnablers();
        loadDocumentTypeImages();
        loadDocumentTypes();
        loadProductFamily();
        loadProduct();
    });

    $('#btnDale').on('click', function () {
        //loadDocuments();
        setFoldersContentType();
    });
});

function saveMetadata(index) {
    if (index == targetDocuments.length) {
        alert('Fin');
        return;
    }

    var document = targetDocuments[index];
    var sourceDoc = findDocument(document.fileName);
    if (sourceDoc != null) {
        var clientContext = SP.ClientContext.get_current();
        var list = clientContext.get_web().get_lists().getByTitle('Documentos B2B');
        var listItem = list.getItemById(document["id"]);

        if (sourceDoc["Familia de producto"] != '')
            listItem.set_item('ProductFamily', sourceDoc["Familia de producto"]);

        if (sourceDoc["Product"] != '')
            listItem.set_item('Product', sourceDoc["Product"]);

        var dc;
        if (sourceDoc["Tipo de documento"] != '') {
            dc = getDocumentType(sourceDoc["Tipo de documento"]);
            listItem.set_item('ContentTypeId', dc.id);
        }
        else {
            dc = getDocumentType("Documento B2B");
            listItem.set_item('ContentTypeId', dc.id);
        }

        var im = getImage(dc.name);
        if (im == null)
            im = documentTypeImages[0];

        listItem.set_item('DocumentTypeImage', im.image);

        if (sourceDoc["Uso"] != '') {
            listItem.set_item('DocumentUse', sourceDoc["Uso"]);
        }

        var enabler = getEnabler(dc.name);
        if (enabler != null) {
            listItem.set_item('Enabler', enabler.enablers);
            $('#documentList').prepend('<p>Elemento:' + document.id + '. DocType: ' + dc.name + '. Enablers: ' + enabler.enablers + '.</p>');
        }
        else {
            $('#documentList').prepend('<p>Elemento:' + document.id + '. DocType: ' + dc.name + '.</p>');
        }

        listItem.update();

        clientContext.executeQueryAsync(
            function () {
                $('#documentList').prepend('<p>Elemento:' + document.id + ' actualizado.</p>');
                var currentIndex = index;
                currentIndex++;
                saveMetadata(currentIndex);
            },
            function (sender, args) {
                var currentIndex = index;
                currentIndex++;
                saveMetadata(currentIndex);

                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            }
        );
    }
    else {
        var currentIndex = index;
        currentIndex++;
        saveMetadata(currentIndex);
    }
}

function findDocument(fileName) {
    var docFound = null;

    for (var i = 0; i < documentsB2B.length; i++) {
        var doc = documentsB2B[i];
        if (doc.File.toLowerCase() == fileName.toLowerCase()) {
            docFound = doc;
            break;
        }
    }

    return docFound;
}

function loadDocuments() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.webAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Documentos B2B');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();

                targetDocuments.push({
                    id: item.get_id(),
                    fileName: item.get_item('FileLeafRef')
                });
            }


            $('#documentList').prepend('<p>Documentos cargados.</p>');

            saveMetadata(0);
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadEnablers() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Enablers');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                var docType = item.get_item('B2BContentType');
                var enabler = item.get_item('Enabler');

                enablers.push({
                    documentType: docType,
                    enablers: enabler
                });
            }

            $('#documentList').prepend('<p>Enablers cargados.</p>');
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadPermissions() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Permisos por tipo de documento');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                var docType = item.get_item('B2BContentType');
                var securityGroup = item.get_item('SecurityGroup');
                var permission = item.get_item('PermissionLevel');
                var permissionLevel;

                switch (permission) {
                    case 'Contributor':
                        permissionLevel = SP.RoleType.contributor;
                        break;
                    case 'Administrator':
                        permissionLevel = SP.RoleType.administrator;
                        break;
                    default:
                        permissionLevel = SP.RoleType.reader;
                        break;
                }

                permissions.push({
                    documentType: docType,
                    securityGroup: securityGroup,
                    permission: permissionLevel
                });
            }
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadDocumentTypeImages() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Imagenes tipos de documento');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                documentTypeImages.push(
                    {
                        documentType: item.get_item('B2BContentType'),
                        image: item.get_item('Image').get_url()
                    }
                );
            }

            $('#documentList').prepend('<p>Imágenes cargadas.</p>');
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadProductFamily() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Familia de productos');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                productFamily.push({
                    id: item.get_id(),
                    name: item.get_item('Title'),
                    code: item.get_item('Code')
                });
            }

            $('#documentList').prepend('<p>Familias de productos cargadas.</p>');
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadProduct() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Productos');
    var query = SP.CamlQuery.createAllItemsQuery();
    var items = list.getItems(query);

    clientContext.load(items);

    clientContext.executeQueryAsync(
        function () {
            var listItemEnumerator = items.getEnumerator();

            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();
                products.push({
                    id: item.get_id(),
                    familyId: item.get_item('ProductFamily').get_lookupId(),
                    name: item.get_item('Title'),
                    code: item.get_item('Code')
                });
            }

            $('#documentList').prepend('<p>Productos cargadas.</p>');
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function loadDocumentTypes() {
    var clientContext = new SP.ClientContext(_spPageContextInfo.webAbsoluteUrl);
    var list = clientContext.get_web().get_lists().getByTitle('Documentos B2B');
    var contentTypes = list.get_contentTypes();

    clientContext.load(contentTypes);

    clientContext.executeQueryAsync(
        function () {
            var results = [];
            var contentTypesEnumerator = contentTypes.getEnumerator();

            while (contentTypesEnumerator.moveNext()) {
                var cType = contentTypesEnumerator.get_current();
                documentTypes.push({
                    id: cType.get_id(),
                    name: cType.get_name()
                });
            }

            $('#documentList').prepend('<p>Document types cargados.</p>');
        },
        function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

function getProduct(familyId) {
    var product = null;

    for (var i = 0; i < products.length; i++) {
        if (products[i].familyId == familyId) {
            product = products[i];
            break;
        }
    }

    return product;
}

function getImage(cType) {
    var image = null;

    for (var i = 0; i < documentTypeImages.length; i++) {
        if (documentTypeImages[i].documentType == cType) {
            image = documentTypeImages[i];
            break;
        }
    }

    return image;
}

function getDocumentType(cType) {
    var ct = null;

    for (var i = 0; i < documentTypes.length; i++) {
        if (documentTypes[i].name.toLowerCase() == cType.toLowerCase()) {
            ct = documentTypes[i];
            break;
        }
    }

    return ct;
}

function getEnabler(cType) {
    var enabler = null;

    for (var i = 0; i < enablers.length; i++) {
        if (enablers[i].documentType.toLowerCase() == cType.toLowerCase()) {
            enabler = enablers[i];
            break;
        }
    }

    return enabler;
}

function dale() {
    if (currentIndex == documentsB2B.length) {
        alert('Fin');
    }
    var sourceDocument = documentsB2B[currentIndex];
    if (sourceDocument["Familia de producto"] != '') {
        //Cogemos el nombre del documento
        var fileName = sourceDocument.File;

        var clientContext = new SP.ClientContext(_spPageContextInfo.webAbsoluteUrl);
        var list = clientContext.get_web().get_lists().getByTitle('Documentos B2B');
        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml("<Where><Contains><FieldRef Name='FileLeafRef'/><Value Type='Text'>" + fileName + "</Value></Contains></Where><RowLimit>1</RowLimit>");

        var items = list.getItems(camlQuery);
        clientContext.load(items);

        clientContext.executeQueryAsync(
            function () {
                var listItemEnumerator = items.getEnumerator();
                while (listItemEnumerator.moveNext()) {
                    var listItem = listItemEnumerator.get_current();

                    listItem.set_item('ProductFamily', sourceDocument["Familia de producto"]);

                    if (sourceDocument["Producto"] != '')
                        listItem.set_item('Product', sourceDocument["Producto"]);

                    if (sourceDocument["Tipo de documento"] != "") {
                        var dc = getDocumentType(sourceDocument["Tipo de documento"]);
                        listItem.set_item('ContentTypeId', dc.id);

                        var im = getImage(sourceDocument["Tipo de documento"]);
                        if (im == null)
                            im = documentTypeImages[0];

                        listItem.set_item('DocumentTypeImage', im.image);
                    }

                    if (sourceDocument["Uso"] != '') {
                        listItem.set_item('DocumentUse', sourceDocument["Uso"]);
                    }

                    $('#documentList').prepend('<p>Elemento:' + listItem.get_id() + '.</p>');

                    listItem.update();

                    clientContext.executeQueryAsync(
                        function () {
                            $('#documentList').prepend('<p>Elemento:' + listItem.get_id() + ' actualizado.</p>');
                            currentIndex++;
                            dale();
                        },
                        function (sender, args) {
                            currentIndex++;
                            dale();

                            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                        }
                    );
                }
            },
            function (sender, args) {
                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            }
        );
    }
}

function setFoldersContentType() {
    var ctx = SP.ClientContext.get_current();
    var list = ctx.get_web().get_lists().getByTitle("Documentos B2B");

    var setFolderInternal = function (parentFolder) {
        var ctx = parentFolder.get_context();
        var curFolders = parentFolder.get_folders();
        ctx.load(curFolders);
        ctx.executeQueryAsync(
            function () {
                var folderEnumerator = curFolders.getEnumerator();
                while (folderEnumerator.moveNext()) {
                    var curFolder = folderEnumerator.get_current();
                    var folderName = curFolder.get_name();

                    $('#documentList').prepend('<p>Elemento:' + folderName + '</p>');

                    var listItem = curFolder.get_listItemAllFields();
                    listItem.set_item('ContentTypeId', '0x0120');

                    listItem.update();

                    ctx.executeQueryAsync(
                        function () {
                            $('#documentList').prepend('<p>' + folderName + ' actualizado.</p>');
                            setFolderInternal(curFolder);
                        },
                        function (sender, args) {
                            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                        }
                    );
                }
            },
            function (sender, args) {
                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            });
    };

    setFolderInternal(list.get_rootFolder());
}