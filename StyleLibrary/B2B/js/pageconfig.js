﻿SP.SOD.executeFunc('sp.js', 'SP.ClientContext', initPageConfig);

function initPageConfig() {
    setTimeout(function () {
        var pageId = _spPageContextInfo.pageItemId;
        var pageListId = _spPageContextInfo.pageListId;

        var context = SP.ClientContext.get_current();
        var web = context.get_web();

        var list = web.get_lists().getById(pageListId);
        var page = list.getItemById(pageId);
        context.load(page);

        context.executeQueryAsync(
            function () {
                var caption = page.get_item('Comments');

                if (caption != null && caption.indexOf('param:') > -1) {
                    var param = caption.split(':');
                    param = param[1].replace(']', '');
                    caption = decodeURIComponent(getUrlParam(param));
                    if (caption == "false")
                        caption = '';

                    $('#page-title').addClass('reverse');
                    $('#page-caption').addClass('reverse');
                }

                if (caption != null && caption.length > 25) {
                    $('#page-caption').css('font-size', '30px');
                }

                $('#page-caption').html(caption);
                $('#page-image').html(page.get_item('PublishingRollupImage'));

                $('#page-title h2').text(translate($('#page-title h2').text()));

                $('#page-title').fadeIn();
                $('#page-caption').fadeIn();
            },
            function () {

            }
        );
    }, 1000);
};