﻿(function ($) {
    $.fn.securityTrimmed = function (options) {
        var $this = this;
        var itemId = options.itemId;
        var permMask = $($this).data('permMask');
        var permissionKind = getPermissionKind(permMask);

        SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () {
            getDocumentPermissions();
        });

        function getDocumentPermissions() {
            var clientContext = new SP.ClientContext(_spPageContextInfo.webAbsoluteUrl);
            var list = clientContext.get_web().get_lists().getByTitle('Documentos B2B');
            var item = list.getItemById(itemId);
            
            clientContext.load(item, 'EffectiveBasePermissions');

            clientContext.executeQueryAsync(
                function () {
                    var hasPermission = item.get_effectiveBasePermissions().has(permissionKind);

                    if (callback != null)
                        callback(hasPermission);
                },
                function (sender, args) {
                    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                }
            );
        }

        function getPermissionKind() {
            switch (permMask) {
                case 'editListItems':
                    return SP.PermissionKind.editListItems;
                case 'viewVersions':
                    return SP.PermissionKind.viewVersions;
                case 'viewListItems':
                    return SP.PermissionKind.viewListItems;
                default:
                    return SP.PermissionKind.emptyMask;
            }
        }
    };
}(jQuery));