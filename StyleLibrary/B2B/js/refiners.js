﻿(function ($) {
    $.fn.refiner = function () {
        var $this = this;
        var refinerCount = 5;
        var refiners;
        var queryText = getUrlParam('k');
        var searchUrl = "https://telefonicacorp.sharepoint.com/sites/B2BFinder.OCEWU/_api/search/query?querytext='" + queryText + "+ListID:39190973-ddd1-426a-9d07-3e5814c9a81e+IsContainer%3dfalse'&refiners='RefinableString01,RefinableString00,RefinableString04,SPContentType,FileType,RefinableString05'"
        var sort = ['SPContentType', 'RefinableString00', 'RefinableString01', 'RefinableString05', 'RefinableString04', 'FileType'];

        function executeSearch() {
            var $ajax = $.ajax({
                url: searchUrl,
                type: "GET",
                dataType: "json",
                headers: {
                    Accept: "application/json;odata=verbose"
                }
            });

            $ajax.done(function (data, textStatus, jqXHR) {
                refiners = data.d.query.PrimaryQueryResult.RefinementResults.Refiners.results;

                refiners = sortArray(refiners);

                //Pintamos el nombre del refinador
                for (var i = 0; i < refiners.length; i++) {
                    var refiner = refiners[i];
                    var entries = refiner.Entries.results;

                    entries.sort(function (a, b) {
                        var textA = a.RefinementName.toUpperCase();
                        var textB = b.RefinementName.toUpperCase();
                        return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                    });

                    var html = '<div class="refiner"><div class="ref-header">';
                    html += translate(refiner.Name);
                    html += '</div>';

                    for (var j = 0; j < entries.length; j++) {
                        var entry = entries[j];
                        html += '<label class="check-container"';

                        //Atributos
                        html += ' data-n="' + refiner.Name + '"';
                        html += ' data-t=' + entry.RefinementToken.toString();
                        html += ' data-o="OR"';
                        html += ' data-k="false"';
                        html += ' data-m="' + entry.RefinementName + '"';

                        if (j > refinerCount - 1)
                            html += ' style="display:none"';

                        html += '>' + translate(entry.RefinementName) + '<span class="badge badge-light float-right">' + entry.RefinementCount + '</span><input type="checkbox"/><span class="checkmark"></span></label>';
                    }

                    if (entries.length > refinerCount)
                        html += '<a href="#" data-command="more">' + translate('ShowMore') + '</a>';

                    html += '</div>';

                    $($this).append(html);
                }

                addClickEvent();
                addShowMoreEvent();
            });
        }

        function addClickEvent() {
            $('.check-container').each(function () {
                $(this).on('click', function () {
                    getRefinerJSON($(this));
                });
            });
        }

        function addShowMoreEvent() {
            $('a[data-command]').on('click', function () {
                var refinerParent = $(this).closest('div.refiner');

                if ($(this).attr('data-command') == 'more') {
                    $(refinerParent).find('label').each(function (index) {
                        if (index > refinerCount - 1) {
                            $(this).fadeIn();
                        }
                    });
                    $(this).text(translate('ShowLess'));
                    $(this).attr('data-command', 'less');
                }
                else {
                    $(refinerParent).find('label').each(function (index) {
                        if (index > refinerCount - 1) {
                            $(this).fadeOut();
                        }
                    });

                    $(this).text(translate('ShowMore'));
                    $(this).attr('data-command', 'more');
                }
            });
        }


        function getRefinerJSON(element) {
            var refinerObjectArray = new Array();

            $('.check-container').each(function () {
                var checkbox = $(this).find('input[type="checkbox"]');
                if ($(checkbox).is(':checked')) {
                    //Comprobamos si el refinador está en el array
                    var refinerName = $(this).data('n');
                    var refinerT = [$(this).data('t')];
                    var refinerO = $(this).data('o');
                    var refinerK = $(this).data('k');
                    var refinerM = new Object();
                    refinerM[$(this).data('t')] = $(this).data('m');

                    var refiner = getRefinerFromArray(refinerObjectArray, refinerName);

                    if (refiner == null) {
                        refiner = new Object();
                        refiner["n"] = refinerName;
                        refiner["t"] = refinerT;
                        refiner["o"] = refinerO;
                        refiner["k"] = refinerK;
                        refiner["m"] = refinerM;

                        refinerObjectArray.push(refiner);
                    }
                    else {
                        refiner["t"].push($(this).data('t'));
                        refiner["m"][$(this).data('t')] = $(this).data('m');
                    }
                }
            });

            createRefinerJSON(refinerObjectArray);
        }

        function sortArray(refinerArray) {
            var newRefinerArray = new Array();

            for (var i = 0; i < sort.length; i++) {
                //Buscamos el refinador
                var refinerName = sort[i];
                var refiner = getRefinerFromResults(refinerArray, refinerName);

                newRefinerArray.push(refiner);
            }

            return newRefinerArray;
        }

        function createRefinerJSON(refinerArray) {
            var refinerJson = new Object();
            refinerJson["k"] = queryText;
            refinerJson["r"] = refinerArray;

            console.log(refinerJson);

            var refinerJsonString = JSON.stringify(refinerJson);

            var url = window.location.href;
            url = url.split('#')[0];
            url += '#Default=' + encodeURIComponent(refinerJsonString);

            window.location.href = url;
        }

        function getRefinerFromResults(resultsArray, refinerName) {
            for (var i = 0; i < resultsArray.length; i++) {
                var refiner = resultsArray[i];
                if (refiner.Name == refinerName)
                    return refiner;
            }

            return null;
        }

        function getRefinerFromArray(refinerArray, refinerName) {
            for (var i = 0; i < refinerArray.length; i++) {
                var refiner = refinerArray[i];
                if (refiner["n"] == refinerName)
                    return refiner;
            }

            return null;
        }


        executeSearch();

    };
}(jQuery));

$(document).ready(function () {
    $('#refiners-panel').refiner();
});