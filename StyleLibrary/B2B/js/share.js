﻿function Share(options) {
    this.element = options.element;
    this.title = options.title;
    var $this = this;

    init = function () {
        var url = $($this.element).data('url');
        if (url == null || url == '')
            url = getUrlParam('doc');

        var template = '<div class="popover share" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>';
        var popover = $($this.element).popover({
            container: 'body',
            content: '',
            template: template,
            html: true,
            placement: 'right',
            title: $this.title
        });

        popover.on('shown.bs.popover', function () {
            $('.popover.share .popover-body').append('<div class="input-group"><input type="text" id="txtUrl" class="form-control" value="' + url + '" /><div class="input-group-append"><span class="input-group-text copy"><span class="icon-copy"></span></span></div></div>');
            $('.popover.share .popover-body').find('.copy').on('click', function () {
                var copyText = document.getElementById("txtUrl");
                copyText.select();
                document.execCommand("copy");
                popover.popover('dispose');

                var tooltip = $($this.element).tooltip({
                    title: 'Enlace copiado',
                    placement: 'right'
                });

                tooltip.tooltip('show');

                setTimeout(function () {
                    tooltip.tooltip('dispose');
                }, 2000);
            });
        });
    };

    init();
}